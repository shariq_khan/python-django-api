from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
import ipaddress

class IsValidIp(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request): 
        try:
            for key, value in request.data.items(): 
                ip = ipaddress.ip_address(value)
            msg = 'Your input: ' + str(ip) + ' is a valid IPv' + str(ip.version) + ' address.'
            return Response({'message': msg}, status.HTTP_200_OK)
        except ValueError:
            msg = 'Please enter an acceptable IPv4 or IPv6 address.'
            return Response({"message": msg}, status.HTTP_400_BAD_REQUEST)