from django.urls import include, path
from parserapp import views

urlpatterns = [
    path('ipaddress-parser', views.IsValidIp.as_view(), name="ipaddress-parser"),
]